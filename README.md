# TensorFlow Examples from R

Code examples showing how **TensorFlow** can be used from within **R** utilizing the **PythonInR** package.
The examples are based on the examples provided at [https://github.com/aymericdamien/TensorFlow-Examples](https://github.com/aymericdamien/TensorFlow-Examples).
