## '''
## HelloWorld example using TensorFlow library.
## Author of the R Code: Florian Schwendinger
## Project: https://bitbucket.org/Floooo/tensorflow-r-examples
## Author of the original Python Code: Aymeric Damien
## Project: https://github.com/aymericdamien/TensorFlow-Examples/
## '''
library(PythonInR)

pyImport("tensorflow", as="tf")

#Simple hello world using TensorFlow

# Create a Constant op
# The op is added as a node to the default graph.
#
# The value returned by the constructor represents the output
# of the Constant op.
hello = tf$constant('Hello, TensorFlow!')

# Start tf session
sess = tf$Session()

sess$run(hello)
